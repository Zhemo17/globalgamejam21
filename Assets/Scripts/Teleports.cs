﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Teleports : MonoBehaviour
{
    public List<Transform> teleporters;

    public void Init(Transform[] teleporters)
    {
        this.teleporters = teleporters.OfType<Transform>().ToList();
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag=="Teleporter")
        {
            AssignTeleport(other.transform);
        }
    }

    public void AssignTeleport(Transform curTeleporter)
    {
        //Debug.Log("Current Teleport "+teleporters[gameObject.GetComponent<MovePlayer>().currentTeleport]);
        int randNumb = Random.Range(0, teleporters.Count);

        if (curTeleporter != teleporters[randNumb] && teleporters[randNumb] != teleporters[GetComponent<MovePlayer>().lastTeleport])
        {
            //Teleport
            transform.position = new Vector3(teleporters[randNumb].position.x, 
                                            teleporters[randNumb].position.y + 3f, 
                                            teleporters[randNumb].position.z);
            //update last teleport
            gameObject.GetComponent<MovePlayer>().lastTeleport = randNumb;
        } 
        else
        {
            AssignTeleport(curTeleporter);
        }
    }
}
