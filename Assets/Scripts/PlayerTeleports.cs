﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.TopDownEngine;

public class PlayerTeleports : MonoBehaviour
{
    [Header("Teleport 1")]
    public Transform[] teleports1 = new Transform[0];

    [Header("Teleport 2")]
    public Transform[] teleports2 = new Transform[0];


    [Header("Cameras")]
    public PlayerFollow camP1;
    public PlayerFollow camP2;

    public static PlayerTeleports Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SearchPlayers());
    }

    IEnumerator SearchPlayers()
    {
        while (true)
        {
            if (LevelManager.Instance.PlayerPrefabs.Length > 1)
            {
                camP1.PlayerTransform = LevelManager.Instance.PlayerPrefabs[0].transform;
                camP2.PlayerTransform = LevelManager.Instance.PlayerPrefabs[1].transform;

                camP1.Init();
                camP2.Init();

                LevelManager.Instance.PlayerPrefabs[0].GetComponent<Teleports>().Init(teleports1);
                LevelManager.Instance.PlayerPrefabs[1].GetComponent<Teleports>().Init(teleports2);

                StopAllCoroutines();
            }

            yield return null;
        }
    }

}
