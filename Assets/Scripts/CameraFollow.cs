﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;

    private Vector3 offset;

    // change this value to get desired smoothness
    public float SmoothTime = 0.3f;

    // This value will change at the runtime depending on target movement. Initialize with zero vector.
    private Vector3 velocity = Vector3.zero;

    public void Init()
    {
        Init2();
        StartCoroutine(UpdateMethod());

    }

    private void Init2()
    {
        offset = transform.position - player.position;
    }

    private void LateUpdate()
    {
        if (player != null)
        {
            // update position
            Vector3 targetPosition = player.position + offset;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);

            // update rotation
            transform.LookAt(player);
        }
    }


    IEnumerator UpdateMethod()
    {
        while (true) {
            if (player != null)
            {/*
                Vector3 newPosition = transform.position;
                newPosition.z = player.position.z;
                newPosition.x = player.position.x;
                transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime / (0.32f * 0.42f));*/
            }

            yield return null;
        }
    }
}
