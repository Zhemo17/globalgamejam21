﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTeleport : MonoBehaviour
{
    public Transform teleporter;
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag=="Player")
        {           
            other.gameObject.transform.position=teleporter.position;
        }
    }
}
