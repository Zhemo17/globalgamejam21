﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    Vector3[] Dir=new Vector3[4];
    public float Speed;
    public string DirectionButtonName;
    public int CurrentDir=0;
    public Animator animController;
    public Transform endPoint;

    bool touchingWall = false;

    public int lastTeleport= 0; 

    // Start is called before the first frame update
    void Start()
    {
        Dir[0]=new Vector3(1,0,0);
        Dir[1]=new Vector3(0,0,-1);
        Dir[2]=new Vector3(-1,0,0);
        Dir[3]=new Vector3(0,0,1);

        StartCoroutine(MoveCorroutine());

    }

    private void Update()
    {
        if (Input.GetButtonDown(DirectionButtonName))
        {
            
            CurrentDir++;
            if (CurrentDir > 3)
                CurrentDir = 0;
            ChangeAnim(CurrentDir);

            touchingWall = false;
           
        }
    }

    // Update is called once per frame
    IEnumerator MoveCorroutine()
    {
        while (!touchingWall) {

            GetComponent<CharacterController>().Move(Dir[CurrentDir] * Speed * Time.deltaTime);
           /* transform.Translate(Dir[CurrentDir] * Speed * Time.deltaTime);*/
            
            yield return null;
        }
    }


    void ChangeAnim(int index)
    {
        switch(index)
        {
            case 0:
                animController.SetTrigger("Forward");
                break;
            case 1:
                animController.SetTrigger("Left");
                break;
            case 2:
                animController.SetTrigger("Back");
                break;
            case 3:
                animController.SetTrigger("Right");
                break; 
            default:
                animController.SetTrigger("Idle");
                break;     
        }
    }
    void OnTriggerEnter(Collider other) 
    {
        Invoke("Teleport",1.0f);
    }

    void Teleport()
    {
        if(endPoint) transform.position=endPoint.position;
    }

}
