﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour {

    public Transform PlayerTransform;

    private Vector3 _cameraOffset;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    public bool LookAtPlayer = false;

    public void Init()
    {
        _cameraOffset = transform.position - PlayerTransform.position;
        StartCoroutine(MethodUpdate());
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// LateUpdate is called after Update
	IEnumerator MethodUpdate () {
        if (true)
        {
            Vector3 newPos = PlayerTransform.position + _cameraOffset;

            transform.position = Vector3.Slerp(transform.position, newPos, SmoothFactor);

            if (LookAtPlayer)
                transform.LookAt(PlayerTransform);

            yield return null;
        }

	}
}
